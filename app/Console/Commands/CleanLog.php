<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\Activitylog\Models\Activity;

class CleanLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:clean-log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all records except latest 1000';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $count = Activity::count();
        Activity::latest()->take($count)->skip(1000)->each(function ($row) {
            $row->delete();
        });

        return 1;
    }
}
