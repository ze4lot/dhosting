<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Activitylog\Traits\LogsActivity;

class Post extends Model
{
    use LogsActivity;

    protected $fillable = ['title', 'content', 'published'];

    protected static $logAttributes = ['title', 'content', 'published'];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param $query
     * @param User $user
     * @return Builder
     */
    public function scopeForUser($query, User $user): Builder
    {
        return $query->where('user_id', $user->id);
    }
}
