<?php

namespace App\Http\Controllers;

use App\Http\Resources\LogCollection;
use App\Post;
use Illuminate\Http\JsonResponse;
use Spatie\Activitylog\Models\Activity;

class LogController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        (new LogCollection(Activity::causedBy(auth()->user())->paginate()))->response();
    }

    /**
     * @param Post $post
     * @return JsonResponse
     */
    public function show(Post $post): JsonResponse
    {
        (new LogCollection(Activity::forSubject($post)->paginate()))->response();
    }
}
