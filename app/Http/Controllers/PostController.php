<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostPublishRequest;
use App\Http\Requests\PostStoreRequest;
use App\Http\Requests\PostUnpublishRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return (new PostCollection(Post::forUser()->paginate()))->response();
    }

    /**
     * @param PostStoreRequest $request
     * @return JsonResponse
     */
    public function store(PostStoreRequest $request): JsonResponse
    {
        return (new PostResource(Post::create($request->validated())))->response();
    }

    /**
     * @param Post $post
     * @return JsonResponse
     */
    public function show(Post $post): JsonResponse
    {
        return (new PostResource($post))->response();
    }

    /**
     * @param Post $post
     * @return JsonResponse
     */
    public function destroy(Post $post): JsonResponse
    {
        try {
            $post->delete();
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()])->setStatusCode(404);
        }

        return response()->json(['message' => 'deleted']);
    }

    /**
     * @param PostPublishRequest $request
     * @param Post $post
     * @return JsonResponse
     */
    public function publish(PostPublishRequest $request, Post $post): JsonResponse
    {
        $post->update(['published' => true]);

        return response()->json(['message' => 'ok']);
    }

    /**
     * @param PostUnpublishRequest $request
     * @param Post $post
     * @return JsonResponse
     */
    public function unpublish(PostUnpublishRequest $request, Post $post): JsonResponse
    {
        $post->update(['published' => false]);

        return response()->json(['message' => 'ok']);
    }
}
