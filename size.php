<?PHP
/**
Zadanie 1

Skrypt tworzy plik określonej wielkości.

cel: 
Zoptymalizowanie skryptu/rozwiązania aby przyspieszyć generowanie pliku.

Polecenie: `php size.php [?args]`

acceptance criteria:
- plik wypełniony randomowymi stringami długości 5-30
- ostatnie linijki pliku mogą mieć mniej znaków, tak aby dopełnić zadany rozmiar
- wielkość pliku musi być co do znaku
- stringi mogą się powatarzać, ale różnych musi być minimum 5 w całym pliku, akceptowalne jest np:
```
(...)
rvhlnuuvpybrloqaz
ruhkmsqwwnz
ruhkmsqwwnz
nzdzzcjnzabeqhvqyzzcvkarzg
ruhkmsqwwnz
ruhkmsqwwnz
ruhkmsqwwnz
hvzpakvwdizitqvxpnbebex
fkqreqwqsufggotxu
ruhkmsqwwnz
(...)
```
**/


function randomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyz';
    $limit = strlen($characters) - 1;
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $limit)];
    }

    return $randomString;
}

$limit = 1024210;
$safeLimit = $limit - 30;

$handle = fopen('generated.txt', 'w+');

while ((int) fstat($handle)['size'] < $safeLimit) {
	fwrite($handle, randomString(random_int(5, 30)) . "\n");
}

while ((int) fstat($handle)['size'] < $limit) {
	fwrite($handle, randomString(1) . "\n");
}

fclose($handle);
