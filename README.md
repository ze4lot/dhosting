# uruchamianie

1. skopiuj .env-example to .env i dostosuj konfiguracje wg. uznania 
2. ```$ composer install```
3. ```$ php artisan key:generate```
4. ```$ php artisan jwt:secret```
5. ```$ php artisan migrate```
*. dane testowe ```$ php artisan db:seed```

reszta z godnie z dokumentacją laravela.

# endpointy api
```$ php artisan route:list```
