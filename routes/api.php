<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->group(function() {
    Route::name('post.index')
        ->get('post', 'PostController@index');
    Route::name('post.show')
        ->get('post/{post}', 'PostController@show');
    Route::name('post.create')
        ->post('post', 'PostController@store');
    Route::name('post.delete')
        ->delete('post/{post}', 'PostController@destroy');
    Route::name('post.publish')
        ->put('post/{post}/publish', 'PostController@publish');
    Route::name('post.unpublish')
        ->put('post/{post}/unpublish', 'PostController@unpublish');

    Route::name('log.index')
        ->get('log', 'LogController@index');
    Route::name('log.show')
        ->get('log/{post}', 'LogController@show');
});

Route::name('register')
    ->post('register', 'JWTAuthController@register');
Route::name('login')
    ->post('login', 'JWTAuthController@login');
