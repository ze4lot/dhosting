<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(User::class, 50)->create();
        $posts = factory(Post::class, 500)->create([
            'user_id' => function() use ($users) {
                return $users->random()->id;
            },
        ]);
    }
}
